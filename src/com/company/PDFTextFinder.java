package com.company;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PDFTextFinder {
    public ArrayList<String> findTextInPDFDocument(String path, List<String> listofstrings)throws IOException{
        ArrayList<String> list= new ArrayList<String>();
        try (PDDocument document = PDDocument.load(new File(path))) {

            if (!document.isEncrypted()) {
                for (String eachstring : listofstrings){
                    boolean found=false;
                    int page=1;
                    // split by whitespace
                    for(int pageNumber =1; pageNumber <= document.getNumberOfPages(); pageNumber++) {

                        PDFTextStripper s = new PDFTextStripper();
                        s.setStartPage(pageNumber);
                        s.setEndPage(pageNumber);
                        String contents = s.getText(document);
                        if (contents.contains(eachstring)) {
                            found=true;
                            page=pageNumber;
                        }
                    }
                    if (found==true){
                        String stringtoprint="Word:'"+eachstring+"',Found:YES,pg:"+page;
                        list.add(stringtoprint);

                    }
                    else
                    {
                        String stringtoprint="Word:'"+eachstring+"',Found:No,pg:-";
                        list.add(stringtoprint);
                    }

                }
            }
            document.close();
        }

        return list;
    }
}
